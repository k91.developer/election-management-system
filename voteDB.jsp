<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<title>투표시스템</title>
<style>

.btn { 
	display:inline-block; 
	text-align:center; 
	vertical-align:middle; 
	text-decoration:none; 
	font-size:12px; 
	color:#000; 
	border:1px 
	solid #000; 
	width:118px; 
	height:38px; 
	line-height:38px;
	margin : 30px;
}

.btn:active{
	background-color:skyblue;	
}

.act{
	background-color:#ff521e;	
}
</style>
</head>
<body>

<center>
<a class="btn" href="enrollDB.jsp" >후보등록</a>
<a class="btn act" href="voteDB.jsp" >투표</a>
<a class="btn" href="resultDB.jsp" >개표결과</a>


<%	
	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	
	ResultSet rset = stmt.executeQuery("select * from hubo_table;");
	ResultSet rs = stmt2.executeQuery("select count(id) from hubo_table;");
	int rows=0;
	while (rs.next()){
		rows = rs.getInt(1);
	}
		out.println("<table cellspacing=0 cellpadding=5px width=700 border=0>");
		out.println("<tr>");
		out.println("<td width=40% align=left><h1>투표</h1></td>");
		out.println("</tr>");
		out.println("</table>");
		out.println("<table cellspacing=0 cellpadding=5px width=700 border=1>");
		out.println("<tr>");
		out.println("<td width=30% align=center> 기호번호 후보자명 </td>");
		out.println("<td width=50% align=center> 연령대 </td>");
		out.println("<td width=20% align=center> 투표 </td>");
		out.println("</tr>");
		
	if(rows!=0){
		out.println("<form method=post action=voteact.jsp>");
		out.println("<tr>");
		out.println("<td><select name=id>");
		out.println("<option> 기호번호 후보자명 </option>");
		while (rset.next()) {
			out.println("<option value="+rset.getInt(1)+"> 기호 " + rset.getInt(1) +"번 "+ rset.getString(2) +"</option>");
		}
		out.println("</select></td>");
				
		out.println("<td><select name=age>");
		out.println("<option> 연령대 </option>");
		for(int age=10; age<=90 ; age+=10){
			out.println("<option value="+age+">" + age+"대  </option>");
		}
		out.println("</select></td>");
		out.println("<td align=center><input type=submit value=투표하기></input></td>");
		out.println("</tr>");
		out.println("</form>");
	
	}else{
		out.println("<tr>");
		out.println("<td colspan=3 align=center>후보자 등록 전입니다.</td>");
		out.println("</tr>");
	}
	out.println("</table>");
	rs.close();
	rset.close();
	stmt2.close();
	stmt.close();
	conn.close();
	
%>
</center>
</body>
</html>