<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<title>투표시스템</title>
<%	
	request.setCharacterEncoding("UTF-8");
	
	int id;
	String ids = request.getParameter("id");
	id = Integer.parseInt(ids);
	
	String name = request.getParameter("name");
%>

<style>
.btn { 
	display:inline-block; 
	text-align:center; 
	vertical-align:middle; 
	text-decoration:none; 
	font-size:12px; 
	color:#000; 
	border:1px 
	solid #000; 
	width:118px; 
	height:38px; 
	line-height:38px;
	margin : 30px;
}

.btn:active{
	background-color:#ff521e;	
}

.act{
	background-color:#ff521e;	
}
</style>
</head>
<body>

<center>
<a class="btn act" href="enrollDB.jsp">후보등록</a>
<a class="btn" href="voteDB.jsp">투표</a>
<a class="btn" href="resultDB.jsp">개표결과</a>

<%	
	out.println("<table cellspacing=0 cellpadding=5px width=700 border=0>");
	out.println("<tr>");
	out.println("<td width=40% align=left><h1>후보 삭제 완료</h1></td>");
	out.println("</tr>");
	out.println("</table>");
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	stmt.execute("delete from hubo_table where id = "+id+";");
	stmt2.execute("delete from tupyo_table where id = "+id+";");
	
	out.println("후보등록 결과 : 기호 "+id+" 번 "+name+" 후보가 삭제되었습니다.");
	stmt2.close();
	stmt.close();
	conn.close();
	
%>

</center>
</body>
</html>