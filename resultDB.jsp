<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page import="java.text.*"%>
<html>
<head>
<title>투표시스템</title>
<style>

.chart div {
  font: 11px sans-serif;
  background-color: orange;
  text-align: right;
  padding: 5px;
  margin: 1px;
  color: blue;  
}

.btn { 
	display:inline-block; 
	text-align:center; 
	vertical-align:middle; 
	text-decoration:none; 
	font-size:12px; 
	color:#000; 
	border:1px 
	solid #000; 
	width:118px; 
	height:38px; 
	line-height:38px;
	margin : 30px;
}

.btn:active{
	background-color:skyblue;	
}

.act{
	background-color:#ff521e;	
}
</style>
</head>

<body>
<center>
<a class="btn" href="enrollDB.jsp">후보등록</a>
<a class="btn" href="voteDB.jsp">투표</a>
<a class="btn act" href="resultDB.jsp">개표결과</a>


<%	
	DecimalFormat format = new DecimalFormat("0.00");
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	
	ResultSet rs1 = stmt1.executeQuery("select count(id) from hubo_table;");
	int row=0;
	while (rs1.next()){
		row = rs1.getInt(1);
	}
		out.println("<table cellspacing=0 cellpadding=5px width=700 border=0>");
		out.println("<tr>");
		out.println("<td width=40% align=left><h1>후보별 득표율</h1></td>");
		out.println("</tr>");
		out.println("</table>");
		out.println("<table cellspacing=0 cellpadding=5px width=800 border=1>");
	if(row!=0){
		ResultSet rset = stmt.executeQuery("select a.id, a.name, count(b.id) from hubo_table a left join tupyo_table b on a.id=b.id group by a.id, a.name;");
		ResultSet rs = stmt2.executeQuery("select count(id) from tupyo_table;");
		double rows=0;
		while (rs.next()){
			rows = rs.getInt(1);
		}
					
		if(rows!=0){
			while (rset.next()) {
				out.println("<tr>");
				out.println("<td width=25% >기호" + rset.getInt(1) +"번 "+
							"<a href=personalresult.jsp?id="+rset.getInt(1)+"&name="+rset.getString(2)+">"+ rset.getString(2) + "</a> 후보자</td>");
				%>
				<td width=65% ><div class="chart">
				<div style="width: calc(95% * <%=rset.getInt(3)/rows%> );"><%=format.format(rset.getInt(3)/rows*100)%>%</div>
				</div></td>
				<%
				out.println("<td width=10% align=center>" + rset.getInt(3)+"("+ format.format(rset.getInt(3)/rows*100)+"%)</td>");
				out.println("</tr>");
			}
		}else{
			out.println("<tr>");
			out.println("<td colspan=4><p align=center>투표 전입니다.</a></p></td>");
			out.println("</tr>");
			
		}
		rs.close();
		rset.close();
	}
	else{
		out.println("<td width=50 colspan=4><p align=center>후보자 등록 전입니다.</a></p></td>");	
	}
		
		out.println("</table>");
		
		rs1.close();
		stmt2.close();
		stmt1.close();
		stmt.close();
		conn.close();
	
	
%>
</center>
</body>
</html>