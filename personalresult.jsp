<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page import="java.text.*"%>
<html>
<head>
<title>투표시스템</title>
<%
	request.setCharacterEncoding("UTF-8");
	
	int id;
	String ids = request.getParameter("id");
	id = Integer.parseInt(ids);
	
	String name = request.getParameter("name");
%>
<style>

.chart div {
  font: 10px sans-serif;
  background-color: orange;
  text-align: right;
  padding: 5px;
  margin: 1px;
  color: blue;
}

.btn { 
	display:inline-block; 
	text-align:center; 
	vertical-align:middle; 
	text-decoration:none; 
	font-size:12px; 
	color:#000; 
	border:1px 
	solid #000; 
	width:118px; 
	height:38px; 
	line-height:38px;
	margin : 30px;
}

.btn:active{
	background-color:skyblue;	
}

.act{
	background-color:#ff521e;	
}

</style>
</head>
<body>

<center>
<a class="btn" href="enrollDB.jsp">후보등록</a>
<a class="btn" href="voteDB.jsp">투표</a>
<a class="btn act" href="resultDB.jsp">개표결과</a>

<%	
	DecimalFormat format = new DecimalFormat("0.00");
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	
	
	ResultSet rs = stmt2.executeQuery("select count(id) from tupyo_table where id="+id+";");
	double rows=0;
	while (rs.next()){
		rows = rs.getInt(1);
	}
		out.println("<table cellspacing=0 cellpadding=5px width=700 border=0>");
		out.println("<tr>");
		out.println("<td width=40% align=left><h1>기호 "+id+"번 "+name+" 후보자 득표성향 분석</h1></td>");
		out.println("</tr>");
		out.println("</table>");
		out.println("<table cellspacing=0 cellpadding=5px width=800 border=1>");
		
	if(rows!=0){
		for(int i=1;i<10;i++){
			ResultSet rset = stmt.executeQuery("select count(*) from tupyo_table where id="+id+" and age = ("+i*10+");");		
				while (rset.next()) {
					out.println("<tr>");
					out.println("<td width=20% align=center>"+i*10+"대</td>");
					%>
					<td width=80% ><div class="chart">
					<div style="width: calc(95% * <%=rset.getInt(1)/rows%> );"><%=rset.getInt(1)%>(<%=format.format(rset.getInt(1)/rows*100)%>%)</div>
					</div></td>
					<%
					out.println("</tr>");
				}
			rset.close();
		}
		
	}else{
		out.println("<tr>");
		out.println("<td colspan=4 align=center>득표수가 없습니다.</td>");
		out.println("</tr>");
		
	}
	out.println("</table>");
	rs.close();
	
	stmt2.close();
	stmt.close();
	conn.close();
	
%>
</center>
</body>
</html>