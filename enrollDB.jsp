<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<title>투표시스템</title>
<style>

.btn { 
	display:inline-block; 
	text-align:center; 
	vertical-align:middle; 
	text-decoration:none; 
	font-size:12px; 
	color:#000; 
	border:1px 
	solid #000; 
	width:118px; 
	height:38px; 
	line-height:38px;
	margin : 30px;
}

.btn:active{
	background-color:skyblue;	
}

.act{
	background-color:#ff521e;	
}
</style>
</head>
<body>

<center>
<a class="btn act" href="enrollDB.jsp">후보등록</a>
<a class="btn" href="voteDB.jsp">투표</a>
<a class="btn" href="resultDB.jsp">개표결과</a>

<%	
	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	
	ResultSet rset = stmt.executeQuery("select * from hubo_table;");
	ResultSet rs = stmt2.executeQuery("select id from hubo_table;");
	int rows=0;
	int row=1;
	while (rs.next()){
		rows = rs.getInt(1);
		if(row==rows){
			row++;
			continue;
			
		} else{
			break;
		}
	}
		out.println("<table cellspacing=0 cellpadding=5px width=700 border=0>");
		out.println("<tr>");
		out.println("<td width=40% align=left><h1> 데이터 조회 </h1></td>");
		out.println("</tr>");
		out.println("</table>");
		out.println("<table cellspacing=0 cellpadding=5px width=700 border=1>");
		out.println("<tr>");
		out.println("<td colspan=3 align=center> 기호번호 </td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td width=40% align=center> 기호번호 </td>");
		out.println("<td width=45% align=center> 후보명 </td>");
		out.println("<td width=15% align=center> 추가/삭제 </td>");
		out.println("</tr>");
	if(rows!=0){
		while (rset.next()) {
			out.println("<tr><form method=post action=delete.jsp?id="+rset.getInt(1)+"&name="+rset.getString(2)+">");
			out.println("<td> 기호번호 : " + rset.getInt(1) + "</td>");
			out.println("<td> 후보명 : " + rset.getString(2) + "</td>");
			out.println("<td align=center><input type=submit value=삭제></input></td>");
			out.println("</form></tr>");
		}
		out.println("<tr><form method=post action=add.jsp>");
		out.println("<td><label> 기호번호 : <input type=number name=id min=1 max=2000 required placeholder="+(row)+"></label></input></td>");
		out.println("<td><label> 후보명 : <input type=text name=name maxlength='20' pattern='^[가-힣a-zA-Z]*$' required></label></input></td>");
		out.println("<td align=center><input type=submit value=추가></input></td>");
		out.println("</form></tr>");
	}else{
		out.println("<tr>");
		out.println("<td colspan=4><p align=center>후보자 없음</a></p></td>");
		out.println("</tr>");
		out.println("<tr><form method=post action=add.jsp>");
		out.println("<td align=left><label> 기호번호 : <input type=number name=id min=1 max=2000 required placeholder="+(row)+"></label></input></td>");
		out.println("<td align=left><label> 후보명 : <input type=text name=name maxlength='20' pattern='^[가-힣a-zA-Z]*$' required></label></input></td>");
		out.println("<td><p align=center><input type=submit value=추가></input></p></td>");
		out.println("</form></tr>");
	}
	out.println("</table>");
	
	rs.close();
	rset.close();
	stmt2.close();
	stmt.close();
	conn.close();
	
%>
</center>
</body>
</html>