<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<title>투표시스템</title>
<head>
<%	
	request.setCharacterEncoding("UTF-8");
	
	int id;
	String ids = request.getParameter("id");
	try{	
		id = Integer.parseInt(ids);
	}catch (Exception e){
		id=0;
	}
	
	int age;
	String ages = request.getParameter("age");
	try{
		age = Integer.parseInt(ages);
	}catch (Exception e){
		age=0;
	}
%>

<style>
.head {
	margin-left : 600px;
}

.btn { 
	display:inline-block; 
	text-align:center; 
	vertical-align:middle; 
	text-decoration:none; 
	font-size:12px; 
	color:#000; 
	border:1px 
	solid #000; 
	width:118px; 
	height:38px; 
	line-height:38px;
	margin : 30px;
}

.back { 
	display:inline-block; 
	text-align:center; 
	vertical-align:middle; 
	text-decoration:none;
	font-size:12px; 
	color:white;
	background-color:black;
	border:1px 
	solid #000; 
	width:80px; 
	height:30px; 
	line-height:30px;
	margin : 5px;
}

.btn:active{
	background-color:skyblue;	
}

.act{
	background-color:#ff521e;	
}
</style>
</head>
<body>
<center>
<a class="btn" href="enrollDB.jsp">후보등록</a>
<a class="btn act" href="voteDB.jsp">투표</a>
<a class="btn" href="resultDB.jsp">개표결과</a>



<%	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	if(id==0||age==0){
		out.println("<h1>투표 실패</h1>");
		out.println("잘못된 데이터입니다. <br> 다시 투표하시길 바랍니다.<br><br>");
		out.println("<a class=back href=voteDB.jsp>돌아가기</a>");
	}else{
		out.println("<h1>투표 완료</h1>");
		stmt.execute("insert into tupyo_table (id,age) values ("+id+","+age+");");
		out.println("투표 결과 : 투표하였습니다.");
	}
	stmt.close();
	conn.close();
	
%>
</center>
</body>
</html>